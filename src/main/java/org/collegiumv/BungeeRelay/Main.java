package org.collegiumv.BungeeRelay;

import com.google.common.io.ByteStreams;
import java.io.File;
import java.io.FileOutputStream;
import org.collegiumv.BungeeRelay.commands.*;
import org.collegiumv.BungeeRelay.listeners.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class Main extends Plugin {

    private static Configuration config = null;

    @Override
    public void onEnable() {
        // Save the default configuration
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }
        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                try (InputStream is = getResourceAsStream("config.yml"); OutputStream os = new FileOutputStream(configFile)) {
                    ByteStreams.copy(is, os);
                }
            } catch (IOException ex) {
                throw new RuntimeException("Unable to create configuration file", ex);
            }
        }
        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        } catch (IOException ex) {
            throw new RuntimeException("Unable to load configuration file", ex);
        }

        // Register listeners
        getProxy().getPluginManager().registerListener(this, new ChatListener(this));
        getProxy().getPluginManager().registerListener(this, new PlayerDisconnectListener());
        getProxy().getPluginManager().registerListener(this, new PostLoginListener());

        // Register commands
        getProxy().getPluginManager().registerCommand(this, new SayCommand());
        getProxy().getPluginManager().registerCommand(this, new PMCommand());
        getProxy().getPluginManager().registerCommand(this, new PMReplyCommand());
        getProxy().getPluginManager().registerCommand(this, new IRCNickCommand());

        // Register aliases
        getProxy().getPluginManager().registerCommand(this, new PMRCommand());

        // Initiate the connection, which will, in turn, pass the socket to the IRC class
        getProxy().getScheduler().runAsync(this, new Runnable() {
            @Override
            public void run() {
                connect();
            }
        });
    }

    public void connect() {
        getLogger().info("Attempting connection...");
        try {
            IRC irc = new IRC(new Socket(config.getString("server.host"), config.getInt("server.port")), config, this);
        } catch (UnknownHostException e) {
            handleDisconnect();
        } catch (IOException e) {
            handleDisconnect();
        }
    }

    public void handleDisconnect() {
        getLogger().info("Disconnected from server.");
        int reconnect = config.getInt("server.reconnect");
        if (reconnect >= 0) {
            getLogger().log(Level.INFO, "Reconnecting in {0} seconds...", reconnect / 1000);
            getProxy().getScheduler().schedule(this, new Runnable() {
                @Override
                public void run() {
                    connect();
                }
            }, reconnect, TimeUnit.MILLISECONDS);
        }
    }
}
